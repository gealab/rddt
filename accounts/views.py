from django.shortcuts import render
from django.contrib.auth.models import Group
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.views.generic import FormView
from django.http import HttpResponseForbidden

from .forms import *
from .models import *



class ProfileView(LoginRequiredMixin,FormView):
    model = CustomUser
    form_class = CustomUserChangeForm
    template_name = 'registration/index-account.html'

    def get_success_url(self):
        return reverse('accounts:index-account', kwargs={})

    def post(self, request, *args, **kwargs):
        
        if not request.user.is_authenticated:
            return HttpResponseForbidden()

        form = self.get_form()

        print(form.errors)
        print(form)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):

        user = form.save()
        user.save()

        return super().form_valid(form)

    def get_object(self):
            return self.request.user
            
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user_obj']  = CustomUser.objects.get(username=self.request.user)
        return context


"""
def index_view(request):
    user_obj = User.objects.get(username=request.user)

     if request.method == 'POST':

        user_form = UserForm(data=request.POST)
        profile_form = UserFormProfile(data=request.POST)

        if all((user_form.is_valid(), profile_form.is_valid())):
            user = user_form.save()
            user.save()
            profile = profile_form.save(commit=False)
            profile.user=user
            profile.save()
            return HttpResponseRedirect(request.path_info)
        else:
            print(user_form.errors)
    else:
        user_form = UserForm()
        profile_form = UserFormProfile()

    
    return render(request,'registration/index-account.html',
                          {'user_form':user_form,
                          'profile_form':profile_form,
                          'user_obj': user_obj})


"""



class CreateView(FormView):
    model = CustomUser
    form_class = CustomUserCreationForm
    template_name = 'registration/registration.html'

    def get_success_url(self):
        return reverse('accounts:index-account', kwargs={})

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        user = form.save()
        user.save()
        group = Group.objects.get(name='user')
        user.groups.add(group)
        login(self.request, user)
        return super().form_valid(form)



"""
def register_view(request):
    registered = False
    if  request.user.is_authenticated:
        return HttpResponseRedirect(reverse('home')) 
    else:
        if request.method == 'POST':
            user_form = UserForm(data=request.POST)
            if user_form.is_valid():
                user = user_form.save()
                user.set_password(user.password)
                user.save()
                group = Group.objects.get(name='user')
                user.groups.add(group)
                login(request, user)

                return HttpResponseRedirect(reverse('home')) 
            else:
                print(user_form.errors)
        else:
            user_form = UserForm()
    

    return render(request,'registration/registration.html',
                          {'user_form':user_form,
                           'registered':registered})
"""