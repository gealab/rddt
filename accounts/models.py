from django.db import models
from django.conf import settings

from django.contrib.auth.models import AbstractUser



class CustomUser(AbstractUser):
    pass
    def __str__(self):
        return self.username

class CustomUserProfileInfo(models.Model):
  user = models.OneToOneField(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
  phone = models.CharField(max_length=20)
  about = models.TextField()


