from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import *
"""
class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta():
        model = User
        fields = ('email','first_name','last_name', 'username')
        

class UserFormProfile(forms.ModelForm):
    class Meta():
        model = UserProfileInfo
        fields = ('phone','about')
"""



class CustomUserCreationForm(UserCreationForm):

    class Meta:
        model = CustomUser
        fields = ('email','first_name','last_name', 'username')

class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = CustomUser
        fields = ('first_name', 'last_name','username','email')
