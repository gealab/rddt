from django.urls import path
from django.conf.urls import include

from .views import *

#from django.contrib.auth import views as auth_views


app_name = 'accounts'
urlpatterns = [
    path('register/',CreateView.as_view(),name='register'),
    path('',ProfileView.as_view(),name='index-account'),

]