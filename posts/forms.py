from django import forms
from django.forms import Textarea,HiddenInput,NumberInput,CharField,IntegerField

from .models import Post, Comment


class PostForm(forms.Form):
    title = forms.CharField()
    text = forms.CharField(widget=forms.Textarea)


class PostModelForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'text']

   

class CommentForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea)


class CommentModelForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text','post','comment']
        widgets = {
            'text': Textarea(attrs={'cols': 44, 'rows': 3,'class': 'w-100'}),
            'post': HiddenInput(),
            'comment': HiddenInput()

        }

   
