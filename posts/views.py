from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.views.generic import View
from django.views.generic.detail import DetailView
from django.utils import timezone
from django.views.generic import FormView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import FormMixin
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.cache import cache_page
from django.core.cache import cache

from .forms import *
from .models import *

class PostDetailView(DetailView,FormMixin):
    model = Post
    form_class = CommentModelForm

    def get_success_url(self):
        return reverse('posts:post-detail', kwargs={'pk': self.object.id})

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid() and request.user.is_authenticated:
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.author = self.request.user
        instance.save()
        return super().form_valid(form)
        
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['comments'] = Comment.objects.filter(post=self.kwargs.get('pk')).order_by('-date')
        return context


class PostListView(ListView):
    model = Post
    paginate_by = 4 
    ordering = ['-date']



class PostCreateView(LoginRequiredMixin,FormView):
    model = Post
    form_class = PostModelForm
    template_name = 'posts/post_create.html'
    def get_success_url(self):
        return reverse('posts:post-list', kwargs={})

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.author = self.request.user
        instance.save()
        
        cache.clear()

        return super().form_valid(form)

