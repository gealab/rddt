from django.urls import path

from .views import *


app_name = 'posts'
urlpatterns = [
    path('', cache_page(60*5)(PostListView.as_view()), name='post-list'),
    #path('', PostListView.as_view(), name='post-list'),
    path('<int:pk>/', PostDetailView.as_view(), name='post-detail'),
    path('create/', PostCreateView.as_view(), name='post-create'),
]