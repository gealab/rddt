from django.shortcuts import render, get_object_or_404, redirect

from .forms import *
from .models import *


class PostDetailMixin(object):
    model = None
    template = None

    def get(self,request,id):
        obj = get_object_or_404(model, id=id)
        comments = Comment.objects.filter(post=id).order_by('-date')
        form = CommentModelForm()

        context = {
            "obj": obj,
            "comments": comments,
            'form': form,
        }
        return render(request, template, context)