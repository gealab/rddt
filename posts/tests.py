import datetime

from django.test import TestCase
from django.utils import timezone

from .models import Post


class PostModelTests(TestCase):
    def test_feature_post(self):
        time = timezone.now() + datetime.timedelta(days=30)
        post = Post(date=time)
        self.assertIs(post.was_published_recently(), False)