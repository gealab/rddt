from django.conf import settings
from django.db import models
import datetime 
from django.db import models
#from datetime import timedelta
from django.utils import timezone
from django.utils.timezone import now


# Create your models here.
class Post(models.Model):
    active = models.BooleanField(default=True)
    title = models.CharField(max_length=100)
    text = models.TextField()
    date =  models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.date <= now
        #return self.date >= timezone.now() - datetime.timedelta(days=1)

class Comment(models.Model):
    text = models.CharField(max_length=200)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    comment = models.ForeignKey("posts.Comment",on_delete=models.CASCADE, blank=True, null=True, related_name='children')
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now)
    
