install env:

python3.7 -m venv rddt

source rddt/bin/activate

pip3.7 install django, django-bootstrap4, psycopg2-binary, django-environ, python-memcached

apt install memcached

postgres:
    psql:
        create database rddt...  WITH ENCODING='UTF8';
        create role rddt_... with encrypted password 'rddt...' login;
        grant all privileges on database rddt... to rddt_...;


mkdir src & cd src

git clone git@gitlab.com:edwardmm/rddt.git

cd rddt 


echo SECRET_KEY=key > rddt/.env && echo DATABASE_URL=postgresql://rddt...:rddt...@127.0.0.1:5432/rddt... >> rddt/.env

python3.7 manage.py makemigrations && python3.7 manage.py migrate

python3.7 manage.py runserver 0.0.0.0:8000
